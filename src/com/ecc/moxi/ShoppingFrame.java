package com.ecc.moxi;

import android.view.LayoutInflater;
import android.view.View;

public class ShoppingFrame extends Frame {
	@Override
	public View onCreateView(LayoutInflater inflater) {
		return inflater.inflate(R.layout.shopping_frame_layout, null);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		initActionBar();
	}
	
	private void initActionBar() {
		getActivity().setupActionBar(false, false, "", getActivity().getString(R.string.cart));
	}

	@Override
	public void fillData() {
		
	}

}
