package com.ecc.moxi;

import android.os.Bundle;

public class LoginActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_layout);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		setupActionBar(true, false, getString(R.string.button_back), getString(R.string.home_sign_in));
	}

	@Override
	protected void onBackEvent() {
		finish();
	}
}
