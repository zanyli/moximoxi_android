package com.ecc.moxi;

import android.view.LayoutInflater;
import android.view.View;

public class CustomServiceFrame extends Frame {
	@Override
	public View onCreateView(LayoutInflater inflater) {
		return inflater.inflate(R.layout.custom_service_frame_layout, null);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		initActionBar();
	}
	
	private void initActionBar() {
		getActivity().setupActionBar(false, false, "", getActivity().getString(R.string.custom_service));
	}

	@Override
	public void fillData() {
		
	}

}
