package com.ecc.moxi.data;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ecc.moxi.HomeShovelerItem;
import com.ecc.moxi.R;

public class SearchAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private List<HomeShovelerItem> mData;
	
	public SearchAdapter(Context context) {
		mInflater = LayoutInflater.from(context);
		mData = new ArrayList<HomeShovelerItem>();
	}
	
	public void update(List<HomeShovelerItem> data) {
		//mData = data;
		mData.clear();
		mData.addAll(data);
	}
	
	public boolean hasData() {
		return mData.size() > 0;
	}

	@Override
	public int getCount() {
		if (hasData())
			return mData.size();
		
		return 1;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.search_dropdown_item, null);
			holder = new ViewHolder();
			
			holder.searchIcon = (ImageView) convertView.findViewById(R.id.search_dropdown_item_icon);
			holder.searchText = (TextView) convertView.findViewById(R.id.search_dropdown_item_text);
			holder.department = (TextView) convertView.findViewById(R.id.search_dropdown_item_department);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		if (hasData()) {
			holder.searchIcon.setVisibility(View.GONE);
		} else {
			holder.searchIcon.setVisibility(View.VISIBLE);
			holder.searchIcon.setBackgroundResource(R.drawable.search_suggestion_barcode);
			holder.searchText.setText(R.string.bc_search_button);
			holder.department.setText("");
		}
		return convertView;
	}
	
	static class ViewHolder {
		ImageView searchIcon;
		TextView searchText;
		TextView department;
	}
}
