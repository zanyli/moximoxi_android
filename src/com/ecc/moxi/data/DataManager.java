package com.ecc.moxi.data;

import java.util.List;

import org.apache.http.HttpRequest;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONObject;

import android.content.Context;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

public class DataManager {
	public interface CategoryListener {
		public void onCategoryChanged(List<CategoryItem> list);
	}
	public interface CategoryGoodsListener {
		public void onCategoryGoodsChanged(List<ProductItem> list);
	}
	public static final String SERVER_BASE = "http://app.moximoxi.net";
	//获取商品分类列表
	public static final String URL_CATEGORY = SERVER_BASE + "category.aspx?";
	//搜索
	public static final String URL_SEARCH = SERVER_BASE + "Search.aspx?";
	//某分类商品列表
	public static final String URL_CATEGORY_GOODS = SERVER_BASE + "CategoryGoods.aspx?";
	//商品轮转
	public static final String URL_SCROLL_ADS = SERVER_BASE + "ScrollAds.aspx?";
	//获取当前中日汇率
	public static final String URL_WEBINFO = SERVER_BASE + "WebInfo.aspx?";
	//获取商品基本信息
	public static final String URL_GOODS = SERVER_BASE + "Goods.aspx?";
	//获取商品评论
	public static final String URL_GOODS_COMMENT = SERVER_BASE + "GoodsComment.aspx?";
	
	private static DataManager instance;
	private RequestQueue mQueue;
	
	private List<CategoryItem> mCategorys;
	private List<ProductItem> mCategoryGoods;
	
	public static synchronized DataManager newInstance(Context context) {
		if (instance == null) 
			instance = new DataManager(context);

		return instance;
	}
	
	private DataManager(Context context) {
		mQueue = Volley.newRequestQueue(context);
	}
	
	public List<CategoryItem> getCategoryList() {
		return mCategorys;
	}
	
	public List<ProductItem> getCategoryGoods() {
		return mCategoryGoods;
	}
	
	public void requestCategoryList(CategoryListener listener) {
		StringBuilder builder = new StringBuilder();
		builder.append(URL_CATEGORY).append("id=0");
		mQueue.add(new JsonObjectRequest(Method.GET, builder.toString(), null, new Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				
			}
		}, null));
	}
	
	public void search(String keyword) {
		StringBuilder builder = new StringBuilder(URL_SEARCH);
		builder.append("page=").append(0).append("&").append("keywork=").append(keyword);
		mQueue.add(new JsonObjectRequest(Method.GET, builder.toString(), null, new Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
				
			}
		}, null));
	}
	
	public void requestCategoryGoods(int categoryId) {
		StringBuilder builder = new StringBuilder(URL_CATEGORY_GOODS);
	}
	
	public static class CategoryItem {
		public int id;
		public String name;
	}
	
	public static class ProductItem {
		//商品唯一编号
		public long id;
		//ProType商品类型，0本站商品，1亚马逊商品，3乐天商品
		public int protype;
		//商品名称
		public String name;
		//商品品牌
		public String brand;
		//商品图片url
		public List<String> urls;
		//商品日元单价
		public String jpyprice;
		//商品人民币单价
		public String cnyprice;
		//商品评论等级
		public int commentLevel;
		//商品评论输了
		public int commentCount;
		//已销售数量
		public int saleOutCount;
		//限定库存
		public int proCount;
		//日本库存数量
		public int japanProCount;
		//中国库存数量
		public int chinaProCount;
		//商品描述
		public String content;
		//商品必选属性
		public String selectAttr;
		//商品标签，在商品名称前显示红色，如新款、直降
		public String tag;
		//广告信息
		public String adsContent;
		//销售状态，1：在售（上架），2：缺货，4：下架，8：删除，16：禁止，32：不支持，64：未审核，128：审核未通过
		public int saleState;
	}

}
