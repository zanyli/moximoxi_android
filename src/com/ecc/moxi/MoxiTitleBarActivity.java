package com.ecc.moxi;

import java.lang.reflect.Method;

import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.TextView;

public class MoxiTitleBarActivity extends FrameActivity {
	public static final int LAYER_TYPE_SOFTWARE = 1;
	protected TextView leftView, centerView;
	protected ViewGroup vg;

	@Override
	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
				R.layout.action_bar_custom_v2);
		
		FrameLayout content = (FrameLayout) findViewById(android.R.id.content);
		
		content.setForeground(getResources().getDrawable(
				R.drawable.skin_header_bar_shadow));
		
		/*if (leftView == null) {
			vg = (ViewGroup) findViewById(R.id.rlCommenTitle);
			setLayerType(vg);
			onCreateLeftView();
			onCreateCenterView();
		}*/
	}
	
	@Override
	public void setTitle(CharSequence title) {
		if (centerView != null && centerView instanceof TextView) {
			TextView titleTextView = (TextView) centerView;
			titleTextView.setText(title);
			super.setTitle(title);
		}
	}
	
	@Override
	public void setTitle(int titleId) {
		String title = getString(titleId);
		setTitle(title);
	}
	
	protected void setLeftViewName(int resId) {
		if (leftView != null && leftView instanceof TextView) {
			String str = getString(resId);
			TextView leftTextView = (TextView) leftView;
			if (str == null || "".equals(str))
				str = getString(R.string.button_back);
			leftTextView.setText(str);
		}
	}
	
	protected void setLeftViewName(String str) {
		if (leftView != null && leftView instanceof TextView) {
			TextView leftTextView = (TextView) leftView;
			if (str == null || "".equals(str))
				str = getString(R.string.button_back);
			leftTextView.setText(str);
		}
	}
	
	protected View onCreateLeftView() {
		leftView = (TextView) findViewById(R.id.ivTitleBtnLeft);
		leftView.setOnClickListener(onBackListeger);
		setLayerType(leftView);
		return leftView;
	}
	
	protected View onCreateCenterView() {
		centerView = (TextView) findViewById(R.id.ivTitleName);
		return centerView;
	}
	
	@Override
	protected void onBackEvent() {
		finish();
	}
	
	protected OnClickListener onBackListeger = new OnClickListener() {
		public void onClick(View v) {
			onBackEvent();
		}
	};
	
	public void enableLeftView(boolean enable) {
		if (enable) {
			leftView.setVisibility(View.VISIBLE);
		} else {
			leftView.setVisibility(View.INVISIBLE);
		}
	}
	
	public static void setLayerType(View view) {
		// I9300title显示有问题，加这段代码
		if (view == null) {
			return;
		}
		if (android.os.Build.VERSION.SDK_INT > 10) {
			Method m;
			try {
				m = view.getClass().getMethod("setLayerType", Integer.TYPE,
						Paint.class);
				m.invoke(view, LAYER_TYPE_SOFTWARE, null);
			} catch (Exception e) {
			}
		}
	}
}
