package com.ecc.moxi;

import com.ecc.moxi.widget.ActionBarViewV2;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;

public abstract class BaseActivity extends Activity {
	
	private ActionBarViewV2 mActionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
	}

	@Override
	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);
		
		mActionBar = (ActionBarViewV2) findViewById(R.id.action_bar_view);
		if (mActionBar == null) return;
		mActionBar.setLeftTitleOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onBackEvent();
			}
		});
	}
	
	protected abstract void onBackEvent();
	
	protected void setupActionBar(boolean enableLeft, boolean showLogo, String leftTitle, String title) {
		if (mActionBar == null) return;
		
		if (enableLeft) {
			mActionBar.enableLeftTitle();
			mActionBar.setLeftTitle(leftTitle);
		} else {
			mActionBar.disableLeftTitle();
		}
		
		if (showLogo) {
			mActionBar.enableTitleLogo();
		} else {
			mActionBar.setTitle(title);
			mActionBar.disableTitleLogo();
		}
	}
	
}
