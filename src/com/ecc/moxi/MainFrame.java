package com.ecc.moxi;

import com.ecc.moxi.widget.HorizontalListView;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;

public class MainFrame extends Frame {
	private static final String TAG = MainFrame.class.getName();
	private HorizontalListView mListView;
	private View mProgressView;
	private View mItemsContainer;
	private HomeShovelerItemsAdapter mAdapter;
	
	private LinearLayout mHomeCategoryBtn;
	private LinearLayout mHomeLoginBtn;
	
	private View mSearchBar;
	
	@Override
	protected void onCreate() {
		super.onCreate();
		
		mItemsContainer = findViewById(R.id.items_horizontal_listview_container);
		mListView = (HorizontalListView) findViewById(R.id.items_horizontal_listview);
		mProgressView = findViewById(R.id.shoveler_view_loading_placeholder);
		
		mHomeCategoryBtn = (LinearLayout) findViewById(R.id.home_product_kind_button);
		mHomeCategoryBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), CategoryBrowseActivity.class);
				startActivity(intent);
			}
		});
		mHomeLoginBtn = (LinearLayout) findViewById(R.id.home_login_or_your_account_button);
		mHomeLoginBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), LoginActivity.class);
				startActivity(intent);
			}
		});
		
		mSearchBar = findViewById(R.id.search_bar_plate);
		mSearchBar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), SearchActivity.class);
				startActivity(intent);
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mAdapter == null) {
			mAdapter = new HomeShovelerItemsAdapter(getActivity());
			mListView.setAdapter(mAdapter);
		}
		if (mAdapter.getCount() > 0) {
			mItemsContainer.setVisibility(View.VISIBLE);
			mListView.setVisibility(View.VISIBLE);
			mProgressView.setVisibility(View.GONE);
		}
		mAdapter.notifyDataSetChanged();
		
		initActionBar();
	}

	@Override
	public View onCreateView(LayoutInflater inflater) {
		return inflater.inflate(R.layout.main_frame_layout, null);
	}
	
	private void initActionBar() {
		getActivity().setupActionBar(false, true, "", "");
		//mActionBar.disableLeftTitle();
		//mActionBar.enableTitleLogo();
	}

	@Override
	public void fillData() {
		
	}

}
