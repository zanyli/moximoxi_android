package com.ecc.moxi;

import java.util.ArrayList;
import java.util.List;

import com.ecc.moxi.widget.ShovelerItemView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class HomeShovelerItemsAdapter extends BaseAdapter {
	private Context mContext;
	private LayoutInflater mInflater;
	private List<HomeShovelerItem> mFullItems;
	public HomeShovelerItemsAdapter(Context context) {
		mContext = context;
		mInflater = LayoutInflater.from(context);
		mFullItems = new ArrayList<HomeShovelerItem>();
		
		for (int i = 0; i < 7; i++) {
			HomeShovelerItem item = new HomeShovelerItem();
			mFullItems.add(item);
		}
	}

	@Override
	public int getCount() {
		return mFullItems.size();
	}

	@Override
	public Object getItem(int position) {
		return mFullItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = (ShovelerItemView) mInflater.inflate(R.layout.home_shoveler_item_view, null);
		}
		((ShovelerItemView) convertView).update(null);
		return convertView;
	}

}
