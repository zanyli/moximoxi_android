package com.ecc.moxi;

import java.util.ArrayList;
import java.util.List;

import com.ecc.moxi.data.SearchAdapter;
import com.google.zxing.client.android.CaptureActivity;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class SearchFrame extends Frame {
	
	private List<HomeShovelerItem> mData;
	
	private ListView mSearchList;
	private SearchAdapter mAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater) {
		return inflater.inflate(R.layout.search_frame_layout, null);
	}
	
	@Override
	protected void onCreate() {
		super.onCreate();
		mData = new ArrayList<HomeShovelerItem>();
		
		mSearchList = (ListView) findViewById(R.id.search_suggestion_list_view);
		mSearchList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (position == 0 && !mAdapter.hasData()) {
					Intent intent = new Intent(getActivity(), CaptureActivity.class);
					startActivity(intent);
				}
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		initActionBar();
		if (mAdapter == null) {
			mAdapter = new SearchAdapter(getActivity());
			mSearchList.setAdapter(mAdapter);
		}
		
		mAdapter.update(mData);
		mAdapter.notifyDataSetChanged();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		mData.clear();
	}

	private void initActionBar() {
		getActivity().setupActionBar(false, false, getActivity().getString(R.string.no),
				getActivity().getString(R.string.search_moxi));
		//mActionBar.enableLeftTitle();
		//mActionBar.setLeftTitle(getActivity().getString(R.string.no));
		
		//mActionBar.disableTitleLogo();
		//mActionBar.setTitle(getActivity().getString(R.string.search_moxi));
	}
	
	@Override
	public void fillData() {
		
	}

}
