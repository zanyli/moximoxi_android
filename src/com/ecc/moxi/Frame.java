package com.ecc.moxi;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;

public abstract class Frame {
	private BaseActivity mActivity;
    private View contentView;
    protected boolean isResume;
    
    protected void onFrameTabClick() {
    }
    
    protected void post(Runnable r) {
    	if(null != contentView) {
    		contentView.post(r);
    	}
    }

    void setActivity(BaseActivity act) {
        this.mActivity = act;
    }

    void setContentView(View content) {
        this.contentView = content;
    }

    public View onCreateView(LayoutInflater inflater) {
        return null;
    }

    /**
     * Return the Activity this frame is currently associated with.
     */
    public final BaseActivity getActivity() {
        return mActivity;
    }

    protected String setLastActivityName() {
        return null;
    }

    // 系统方法

    protected void onCreate() {
        //updateAppRuntime();
    }

    protected void onResume() {
        isResume = true;
    }

    protected void onPause() {
        isResume = false;
    }

    protected void onDestroy() {

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    public void onConfigurationChanged(Configuration newConfig) {

    }

    
    /**
     * 这个是切换帐号后会走一次的数据加载，没必要去重写onAccountChanged了。。直接在里面加好就好，
     * app的话在updateAppRuntime()已经换好了，所以不用自己换。add by siweizhou
     * */
    public abstract void fillData();

    /**
     * 能不重写这货就不要重写了，有坑。。你一定要super()，不然麻烦了。可以重写
     * fillData（），fillData这个虽然名字恶心，至少保证不会错。add by siweizhou
     * 这里不改成final真是可惜，消息tab和设置不follow这里的设计，联系人和动态就很好嘛.
     * */
    protected void onAccountChanged() {
        fillData();
    }

    public Resources getResources() {
        return mActivity.getResources();
    }

    public View findViewById(int id) {
        return contentView.findViewById(id);
    }

    public void startActivity(Intent intent) {
        mActivity.startActivity(intent);
    }

    public void startActivityForResult(Intent intent, int requestCode) {
        mActivity.startActivityForResult(intent, requestCode);
    }

    public String getString(int resId) {
        return mActivity.getString(resId);
    }

    public void runOnUiThread(Runnable action) {
        mActivity.runOnUiThread(action);
    }

    public Object getSystemService(String name) {
        return mActivity.getSystemService(name);
    }

    public ContentResolver getContentResolver() {
        return mActivity.getContentResolver();
    }

    public void finish() {
        mActivity.finish();
    }
    
    public boolean onBackPressed() {
        return false;
    }

	public void onDrawComplete(){
		//TODO
	}

}
