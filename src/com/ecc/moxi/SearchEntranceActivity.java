package com.ecc.moxi;

import android.os.Bundle;
import android.view.View;

public abstract class SearchEntranceActivity extends BaseActivity {
	
	private View mSearchEntrance;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);
		
		mSearchEntrance = findViewById(R.id.search_bar_plate);
		mSearchEntrance.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				onClickSearchEntrance();
			}
		});
	}
	
	public abstract void onClickSearchEntrance();

}
