package com.ecc.moxi;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CategoryBrowseSubActivity extends SearchEntranceActivity {

	private LinearLayout mPicGallery;
	private LinearLayout mCategoryList;

	private String[] mData;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.category_browse_sub_layout);

		mPicGallery = (LinearLayout) findViewById(R.id.category_product_gallery);
		mCategoryList = (LinearLayout) findViewById(R.id.category_subitems_list);

		mData = getResources().getStringArray(R.array.category_sub_array);
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		setupActionBar(true, false, getString(R.string.button_back), getString(R.string.categroy_browse_department));
		
		mCategoryList.addView(getSubAllDepartmentItemView());
		for (String str : mData) {
			mCategoryList.addView(getSubDepartmentItemView(str));
			mPicGallery.addView(getTopItemView());
		}
	}

	private View getSubDepartmentItemView(String str) {
		View view = LayoutInflater.from(this).inflate(
				R.layout.category_browse_item, null);
		view.setClickable(true);
		TextView textview = (TextView) view.findViewById(R.id.category_subitem);
		textview.setText(str);
		return view;
	}
	
	private View getSubAllDepartmentItemView() {
		View view = LayoutInflater.from(this).inflate(
				R.layout.category_browse_item, null);
		view.setClickable(true);
		TextView textview = (TextView) view.findViewById(R.id.category_subitem);
		textview.setVisibility(View.INVISIBLE);
		TextView allView = (TextView) view.findViewById(R.id.all_category_subitem);
		allView.setVisibility(View.VISIBLE);
		return view;
	}
	
	private ImageView getTopItemView() {
		ImageView view = new ImageView(this);
		view.setBackgroundResource(R.drawable.ic_launcher);
		view.setLayoutParams(new LayoutParams(200, 200));
		return view;
	}

	@Override
	protected void onBackEvent() {
		finish();
	}

	@Override
	public void onClickSearchEntrance() {
		Intent intent = new Intent(this, SearchActivity.class);
		startActivity(intent);
	}
}
