package com.ecc.moxi;

public class HomeShovelerItem {
	private String imageUrl;
	private String title;
	private int imagesCount;
	private String listPrice;
	private String tradeInPrice;
	private String productGroupId;
	private String summary;

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getImagesCount() {
		return imagesCount;
	}

	public void setImagesCount(int imagesCount) {
		this.imagesCount = imagesCount;
	}

	public String getListPrice() {
		return listPrice;
	}

	public void setListPrice(String listPrice) {
		this.listPrice = listPrice;
	}

	public String getTradeInPrice() {
		return tradeInPrice;
	}

	public void setTradeInPrice(String tradeInPrice) {
		this.tradeInPrice = tradeInPrice;
	}

	public String getProductGroupId() {
		return productGroupId;
	}

	public void setProductGroupId(String productGroupId) {
		this.productGroupId = productGroupId;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

}
