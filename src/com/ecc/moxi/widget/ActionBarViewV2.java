package com.ecc.moxi.widget;

import com.ecc.moxi.BaseActivity;
import com.ecc.moxi.FrameActivity;
import com.ecc.moxi.R;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ActionBarViewV2 extends RelativeLayout {
	private TextView mCenterTitle;
	private TextView mLeftTitle;
	private ImageView mLogo;
	private final BaseActivity mActivity;

	public ActionBarViewV2(Context paramContext, AttributeSet paramAttributeSet) {
		super(paramContext, paramAttributeSet);
		this.mActivity = ((BaseActivity) paramContext);
	}
	
	public void setTitle(CharSequence title) {
		mCenterTitle.setText(title);
	}
	
	public void setLeftTitle(String text) {
		mLeftTitle.setText(text);
	}
	
	public void enableLeftTitle() {
		mLeftTitle.setVisibility(View.VISIBLE);
	}
	
	public void disableLeftTitle() {
		mLeftTitle.setVisibility(View.INVISIBLE);
	}
	
	public void enableTitleLogo() {
		mLogo.setVisibility(View.VISIBLE);
		mCenterTitle.setVisibility(View.INVISIBLE);
	}
	
	public void disableTitleLogo() {
		mLogo.setVisibility(View.INVISIBLE);
		mCenterTitle.setVisibility(View.VISIBLE);
	}

	@Override
	protected void onFinishInflate() {
		mLogo = (ImageView) findViewById(R.id.titleLogo);
		mCenterTitle = (TextView) findViewById(R.id.ivTitleName);
		mLeftTitle = (TextView) findViewById(R.id.ivTitleBtnLeft);
	}
	
	public void setLeftTitleOnClickListener(OnClickListener lis) {
		mLeftTitle.setOnClickListener(lis);
	}

}
