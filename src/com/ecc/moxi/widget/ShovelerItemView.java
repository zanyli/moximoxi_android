package com.ecc.moxi.widget;

import com.ecc.moxi.R;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/*
 * 加载图片每个对于的图片*/
public class ShovelerItemView extends LinearLayout {
	private Drawable mDrawable;
	private String mImageUrl;
	private ImageView mThumbnail;
	private TextView mTitleView;
	private TextView mPriviceView;

	public ShovelerItemView(Context paramContext, AttributeSet paramAttributeSet) {
		super(paramContext, paramAttributeSet);
	}

	public void update(String paramString) {
		this.mImageUrl = paramString;
		
		if (mThumbnail == null) {
			mThumbnail = (ImageView) findViewById(R.id.home_shoveler_item_thumbnail);
		}
		if (mTitleView == null) {
			mTitleView = (TextView) findViewById(R.id.home_shoveler_item_title);
		}
		if (mPriviceView == null) {
			mPriviceView = (TextView) findViewById(R.id.home_shoveler_item_item_price);
		}
		
		mThumbnail.setBackgroundResource(R.drawable.ic_launcher);
		mTitleView.setText("3M 食品保鲜待 立体带型 韩国原装进口 赶快来体验狗眼！");
		mPriviceView.setText("46.00");
		
	}
}
