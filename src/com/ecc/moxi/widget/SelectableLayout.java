package com.ecc.moxi.widget;

import com.ecc.moxi.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;

public class SelectableLayout extends LinearLayout {
	private static final int MOVE_THRESHOLD = 10;
	private float mDownPosY = 0.0F;
	private boolean mMoveOccured = false;

	public SelectableLayout(Context paramContext) {
		super(paramContext);
	}

	public SelectableLayout(Context paramContext, AttributeSet paramAttributeSet) {
		super(paramContext, paramAttributeSet);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			setEffectPressed(true);
			break;
		case MotionEvent.ACTION_UP:
			setEffectPressed(false);
			break;
		default:
			break;
		}
		return super.onTouchEvent(event);
	}

	protected void setEffectPressed(boolean paramBoolean) {
		if (paramBoolean) {
			int i = getPaddingLeft();
			int j = getPaddingTop();
			int k = getPaddingRight();
			int m = getPaddingBottom();
			setBackgroundResource(R.drawable.orange_outline_sharp_corner);
			setPadding(i, j, k, m);
			return;
		}
		setBackgroundColor(getResources().getColor(17170445));
	}
}
