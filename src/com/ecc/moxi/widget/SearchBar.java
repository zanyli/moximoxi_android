package com.ecc.moxi.widget;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ecc.moxi.FrameActivity;
import com.ecc.moxi.R;

public abstract class SearchBar extends LinearLayout {
	private FrameActivity mActivity;
	private boolean mIsFirstTimeEnter;
	private boolean mIsTapped = false;
	private View mSearchBar;
	private View mSearchBarPlate;
	private ImageView mSearchBarcode;
	private EditText mSearchInput;

	public SearchBar(Context paramContext) {
		super(paramContext);
		this.mActivity = ((FrameActivity) paramContext);
		this.mIsFirstTimeEnter = true;
		initSearchBar();
	}

	public SearchBar(Context paramContext, AttributeSet paramAttributeSet) {
		super(paramContext, paramAttributeSet);
		this.mActivity = ((FrameActivity) paramContext);
		this.mIsFirstTimeEnter = true;
	}

	protected void doBarcodeScan() {
		/*Intent localIntent = getBarcodeScanIntent();
		this.mActivity.startActivity(localIntent);*/
	}

	protected Intent getBarcodeScanIntent() {
		/*Intent localIntent = new Intent(getContext(), SearchActivity.class);
		localIntent.setAction("android.intent.action.VIEW");
		localIntent.setData(Uri.parse("BARCODE_SEARCH_QUERY_KEYWORD"));
		return localIntent;*/
		return null;
	}

	public View getSearchBar() {
		return this.mSearchBar;
	}

	public ImageView getSearchBarcodeView() {
		return this.mSearchBarcode;
	}

	public EditText getSearchInputView() {
		return this.mSearchInput;
	}

	public TextWatcher getTextWatcher() {
		return new TextWatcher() {
			@Override
			public void afterTextChanged(Editable paramAnonymousEditable) {
				
			}

			public void beforeTextChanged(
					CharSequence paramAnonymousCharSequence,
					int paramAnonymousInt1, int paramAnonymousInt2,
					int paramAnonymousInt3) {
			}

			public void onTextChanged(CharSequence paramAnonymousCharSequence,
					int paramAnonymousInt1, int paramAnonymousInt2,
					int paramAnonymousInt3) {
			}
		};
	}

	protected void goToSearchEntry(String paramString, boolean paramBoolean) {
		//this.mActivity.onSearchRequested(paramString, paramBoolean);
	}

	protected void initSearchBar() {
		this.mSearchBar = LayoutInflater.from(this.mActivity).inflate(
				R.layout.search_bar_entrance, null);
		this.mSearchBar.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
		//mSearchInput = ((EditText)this.mSearchBar.findViewById(R.id.search_edit_text));
		addView(this.mSearchBar);
		/*if (GNODebugSettingsActivity.useNewGNO()) {
			((ImageView) this.mSearchBar.findViewById(R.id.search_mag_glass))
					.setImageResource(R.drawable.search_bar_mag_glass);
		}*/

		this.mSearchInput.addTextChangedListener(getTextWatcher());
		//checkPreviousSearchTerm();
	}

	public boolean isTapped() {
		return this.mIsTapped;
	}

	public abstract void logRefMarker();

	protected void onFinishInflate() {
		super.onFinishInflate();
		initSearchBar();
	}

	public void onWindowFocusChanged(boolean paramBoolean) {
		if (paramBoolean) {
			if (this.mIsFirstTimeEnter) {
				this.mIsFirstTimeEnter = false;
				requestFocus();
			}
			//checkPreviousSearchTerm();
		}
	}

	protected void startSearch() {
		Editable localEditable = this.mSearchInput.getText();
		/*if (localEditable != null) {
			String str = AdvSearchResultsBrowser.validateQuery(localEditable
					.toString());
			if (!Util.isEmpty(str)) {
				Activity localActivity = (Activity) getContext();
				Intent localIntent = new Intent(localActivity,
						SearchActivity.class);
				localIntent.addFlags(131072);
				localIntent.setData(Uri
						.parse(SearchActivity.makeQuery("paramText", str))
						.buildUpon().build());
				localActivity.startActivity(localIntent);
			}
		}*/
	}
}
