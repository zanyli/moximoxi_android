package com.ecc.moxi.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TabHost;

public class MoxiTabHost extends TabHost {
	
	private OnTabSelectionListener mOnTabSelectionListener;
	private boolean isFirstDraw = false;

	public MoxiTabHost(Context context) {
		super(context);
		setWillNotDraw(false);
	}

	public MoxiTabHost(Context context, AttributeSet attrs) {
		super(context, attrs);
		setWillNotDraw(false);
	}
	
	public void setFirstDrawTrue(){
		isFirstDraw = true;
	}
	
	public static interface OnTabSelectionListener {
		public void onTabSelected(int pre, int cur, MoxiTabHost from);
	}

	public void setOnTabSelectionListener(OnTabSelectionListener l) {
		mOnTabSelectionListener = l;
	}
	
	public void setCurrentTab(int index) {
		int count = getTabWidget().getTabCount();
		if(index < 0 || index >= count) {
			return;
		}
		
		int pre = getCurrentTab();
		
		super.setCurrentTab(index);
		if(null != mOnTabSelectionListener) {
			mOnTabSelectionListener.onTabSelected(pre, index, this);
		}
	}
}
