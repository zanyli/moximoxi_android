package com.ecc.moxi;

import java.util.ArrayList;
import java.util.List;

import com.ecc.moxi.data.SearchAdapter;
import com.google.zxing.client.android.CaptureActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class SearchActivity extends SearchEntranceActivity {
	
	private List<HomeShovelerItem> mData;
	private LayoutInflater mInflater;
	
	private ListView mSearchList;
	private SearchAdapter mAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_layout);
		
		mData = new ArrayList<HomeShovelerItem>();
		
		mSearchList = (ListView) findViewById(R.id.search_suggestion_list_view);
		mSearchList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (position == 0 && !mAdapter.hasData()) {
					Intent intent = new Intent(SearchActivity.this, CaptureActivity.class);
					startActivity(intent);
				}
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		setupActionBar(true, false, getString(R.string.no),
				getString(R.string.search_moxi));
		
		if (mAdapter == null) {
			mAdapter = new SearchAdapter(this);
			mSearchList.setAdapter(mAdapter);
		}
		
		mAdapter.update(mData);
		mAdapter.notifyDataSetChanged();
	}

	@Override
	protected void onBackEvent() {
		finish();
	}

	@Override
	public void onClickSearchEntrance() {
		
	}

}
