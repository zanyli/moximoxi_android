package com.ecc.moxi;

import com.ecc.moxi.widget.ActionBarViewV2;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class CategoryBrowseActivity extends BaseActivity {
	
	private LayoutInflater mInflater;
	private String[] mData;
	
	private CategoryAdapter mAdapter;
	private ListView mListView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.category_browse_layout);

		mListView = (ListView) findViewById(R.id.category_browse_list);
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent intent = new Intent(CategoryBrowseActivity.this, CategoryBrowseSubActivity.class);
				startActivity(intent);
			}
		});
		
		mData = getResources().getStringArray(R.array.category_array);
		mInflater = LayoutInflater.from(this);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		if (mAdapter == null) {
			mAdapter = new CategoryAdapter();
			mListView.setAdapter(mAdapter);
		} else {
			mAdapter.notifyDataSetChanged();
		}
		
		setupActionBar(true, false, getString(R.string.home), getString(R.string.categroy_browse_department));
		//initActionBar();
	}

	private class CategoryAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			if (mData == null) return 0;
			return mData.length;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				holder = new ViewHolder();
				
				convertView = mInflater.inflate(R.layout.category_browse_item, null);
				holder.text = (TextView) convertView.findViewById(R.id.category_subitem);
				
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			
			holder.text.setText(mData[position]);
			return convertView;
		}
		
	}
	
	static class ViewHolder {
		TextView text;
	}

	@Override
	protected void onBackEvent() {
		finish();
	}

}
