package com.ecc.moxi;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class MoreFrame extends Frame implements OnClickListener {

	@Override
	protected void onCreate() {
		super.onCreate();

		//最新公告、代购单提交、领取优惠券
		initItem(R.id.more_sub_latest_announce, R.string.more_frame_latest_announce, R.drawable.more_frame_top_item_selector);
		initItem(R.id.more_sub_daigou, R.string.more_frame_dagou, R.drawable.more_frame_middle_item_selector);
		initItem(R.id.more_sub_receive_youhui, R.string.more_frame_receive_youhui, R.drawable.more_frame_bottom_item_selector);
		//<!-- 我的订单、我的信息、我的代购、收获地址 -->
		initItem(R.id.more_sub_my_shopping, R.string.more_frame_my_shopping, R.drawable.more_frame_top_item_selector);
		initItem(R.id.more_sub_my_info, R.string.more_frame_my_info, R.drawable.more_frame_middle_item_selector);
		initItem(R.id.more_sub_my_daigou, R.string.more_frame_my_daigou, R.drawable.more_frame_middle_item_selector);
		initItem(R.id.more_sub_address, R.string.more_frame_address, R.drawable.more_frame_bottom_item_selector);
		//<!-- 我的收藏 -->
		initItem(R.id.more_sub_my_collect, R.string.more_frame_my_collect, R.drawable.more_frame_middle_item_selector);
		//<!-- 写评论、我的评论、摩币明细 -->
		initItem(R.id.more_sub_write_comment, R.string.more_frame_write_comment, R.drawable.more_frame_top_item_selector);
		initItem(R.id.more_sub_my_comment, R.string.more_frame_my_comment, R.drawable.more_frame_middle_item_selector);
		initItem(R.id.more_sub_mobi_detail, R.string.more_frame_mobi_detail, R.drawable.more_frame_bottom_item_selector);
		//<!-- 我的任务 -->
		initItem(R.id.more_sub_my_task, R.string.more_frame_my_task, R.drawable.more_frame_middle_item_selector);
		//<!-- 我的背包、退换货管理、帮助中心 -->
		initItem(R.id.more_sub_my_bag, R.string.more_frame_my_bag, R.drawable.more_frame_top_item_selector);
		initItem(R.id.more_sub_goods_managment, R.string.more_frame_goods_managment, R.drawable.more_frame_middle_item_selector);
		initItem(R.id.more_sub_help_center, R.string.more_frame_help_center, R.drawable.more_frame_bottom_item_selector);
		// <!-- 联系我们、法规-->
		initItem(R.id.more_sub_contact_us, R.string.more_frame_contact_us, R.drawable.more_frame_top_item_selector);
		initItem(R.id.more_sub_law, R.string.more_frame_law, R.drawable.more_frame_bottom_item_selector);
	}

	@Override
	protected void onResume() {
		super.onResume();
		initActionBar();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public View onCreateView(LayoutInflater inflater) {
		return inflater.inflate(R.layout.more_frame_layout, null);
	}

	@Override
	public void fillData() {

	}
	
	private void initActionBar() {
		getActivity().setupActionBar(false, false, "", getActivity().getString(R.string.more));
	}

	public void initItem(int layoutId, int stringId, int backgroundId) {
		View view = findViewById(layoutId);
		if (view == null) {
			return;
		}
		TextView tv = ((TextView) view.findViewById(R.id.qqsetting_item_text));
		tv.setText(stringId);
		view.setOnClickListener(this);
		view.setBackgroundResource(backgroundId);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.more_sub_latest_announce:
		case R.id.more_sub_daigou:
		case R.id.more_sub_receive_youhui:
		case R.id.more_sub_my_shopping:
		case R.id.more_sub_my_info:
		case R.id.more_sub_my_daigou:
		case R.id.more_sub_address:
		case R.id.more_sub_my_collect:
		case R.id.more_sub_write_comment:
		case R.id.more_sub_my_comment:
		case R.id.more_sub_mobi_detail:
		case R.id.more_sub_my_task:
		case R.id.more_sub_my_bag:
		case R.id.more_sub_goods_managment:
		case R.id.more_sub_help_center:
		case R.id.more_sub_contact_us:
		case R.id.more_sub_law:
			Toast.makeText(getActivity(), "请在MoreFrame.java onClick()方法实现具体跳转", Toast.LENGTH_LONG).show();
			break;
		default:
			break;
		}
	}

}
