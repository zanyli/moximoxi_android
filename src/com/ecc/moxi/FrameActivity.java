package com.ecc.moxi;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.ecc.moxi.widget.ActionBarViewV2;
import com.ecc.moxi.widget.MoxiTabHost;
import com.ecc.moxi.widget.MoxiTabHost.OnTabSelectionListener;

import android.app.Activity;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.Window;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabContentFactory;

public class FrameActivity extends BaseActivity implements OnTabChangeListener, TabContentFactory {
	
	protected TabHost mTabHost;
    private final Map<String, Frame> mFrames = new HashMap<String, Frame>(4);
    private Frame preFrame;
    
    @Override
	protected void onCreate(Bundle savedInstanceState) {
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
	}
    
    @Override
	protected void onResume() {
		super.onResume();
        Frame frame = getCurrentFrame();
		if(frame != null) {
			frame.onResume();
		}
    }
    
    @Override
    protected void onPause() {
    	super.onPause();
        Frame frame = getCurrentFrame();
		if(frame != null) {
			frame.onPause();
		}
    }

    protected String setLastActivityName() {
        Frame frame = getCurrentFrame();
        if(frame != null) {
            return frame.setLastActivityName();
        }
        return null;
    }

    protected void addFrame(View contentView, Class<? extends Frame> clz, View tab) {
        if (mTabHost == null) {
            if(contentView != null) {
                mTabHost = (TabHost) contentView.findViewById(android.R.id.tabhost);
            } else {
                mTabHost = (TabHost) findViewById(android.R.id.tabhost);
            }
            mTabHost.setup();
            mTabHost.setOnTabChangedListener(this);
            if(mTabHost instanceof MoxiTabHost) {
            	MoxiTabHost tabHost = (MoxiTabHost) mTabHost;
            	tabHost.setOnTabSelectionListener(new OnTabSelectionListener() {

					@Override
					public void onTabSelected(int pre, int cur, MoxiTabHost from) {
						if(pre == cur) {
							//只放开需求的部分
							Frame f = getCurrentFrame();
							if(null != f) {
								f.onFrameTabClick();
							}
						}
					}
				});
            }
        }
        mTabHost.addTab(mTabHost.newTabSpec(clz.getName()).setIndicator(tab).setContent(this));
    }
    
	/**
	 * 返回titlebar的高度 in pixels
	 * @return
	 */
    public int getTitleBarHeight() {
    	return getResources().getDimensionPixelSize(R.dimen.title_bar_height);
    }

    long tabBeginActionTime = -1;
    String tabName = null;
    
    @Override
    public void onTabChanged(String tabId) {
    	if(tabId != null){
        	tabName = tabId;
        	int index = tabId.lastIndexOf(".");
        	if(index >= 0){
        		tabName = tabId.substring(index+1, tabId.length());
        	}
        	tabBeginActionTime = SystemClock.uptimeMillis();
    	}
        if (preFrame != null) {
            preFrame.onPause();
        }
        
        preFrame = getCurrentFrame();
        if(preFrame != null) {
        	preFrame.onResume();
        }
    }

    protected Frame getCurrentFrame() {
    	if(mTabHost == null) {
    		return null;
    	}
        return mFrames.get(mTabHost.getCurrentTabTag());
    }
    @Override
    protected void onDestroy() {
    	super.onDestroy();
        Collection<Frame> all = mFrames.values();
        for (Frame frame : all) {
            frame.onDestroy();
        }
    }

    public int getCurrentTab() {
        return mTabHost.getCurrentTab();
    }
    
    @Override
    protected void onBackEvent() {
	}

	@Override
	public View createTabContent(String tag) {
		final String className = tag;
        View content = null;
        Frame frame = null;
        try {
            frame = (Frame) Class.forName(className).newInstance();
        } catch (Exception e) {
            return null;
        }
        frame.setActivity(this);
        content = frame.onCreateView(getLayoutInflater());
        frame.setContentView(content);
        frame.onCreate();
        mFrames.put(className, frame);
        return content;
	}

}
