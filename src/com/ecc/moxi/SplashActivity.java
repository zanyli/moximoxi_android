package com.ecc.moxi;

import java.util.HashMap;
import java.util.Map;

import com.ecc.moxi.widget.MoxiTabWidget;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

public class SplashActivity extends FrameActivity {

	public static final String TAG = "SplashActivity";

	public static final int MainTab = 0;
	public static final int SearchTab = 1;
	public static final int ShoppintCartTab = 2;
	public static final int CustomServiceTab = 3;
	public static final int MoreTab = 4;

	public static final String TAB_INDEX = "tab_index";

	public static final String TAB_TAG_MAIN = "首页";
	public static final String TAB_TAG_SEARCH = "搜索";
	public static final String TAB_TAG_SHOPPINTCART = "购物车";
	public static final String TAB_TAG_CUSTOMSERVICE = "客服";
	public static final String TAB_TAG_MORE = "更多";

	private View[] mTabs = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		initTabs();
		
		mTabHost.setCurrentTab(0);
	}

	private void initTabs() {
		View tab1 = createTabItem(0);
		View tab2 = createTabItem(1);
		View tab3 = createTabItem(2);
		View tab4 = createTabItem(3);
		View tab5 = createTabItem(4);

		mTabs = new View[] { tab1, tab2, tab3, tab4, tab5};

		MoxiTabWidget tabWidget = (MoxiTabWidget) findViewById(android.R.id.tabs);

		addFrame(null, MainFrame.class, mTabs[0]);
		addFrame(null, SearchFrame.class, mTabs[1]);
		addFrame(null, ShoppingFrame.class, mTabs[2]);
		addFrame(null, CustomServiceFrame.class, mTabs[3]);
		addFrame(null, MoreFrame.class, mTabs[4]);
	}

	private View createTabItem(int bgId) {
		View tabItem = getLayoutInflater().inflate(
				R.layout.mainactivity_tab_item, null);
		TextView view = (TextView) tabItem.findViewById(R.id.tab_label);
		switch (bgId) {
		case MainTab:
			view.setText(TAB_TAG_MAIN);
			break;
		case SearchTab:
			view.setText(TAB_TAG_SEARCH);
			break;
		case ShoppintCartTab:
			view.setText(TAB_TAG_SHOPPINTCART);
			break;
		case CustomServiceTab:
			view.setText(TAB_TAG_CUSTOMSERVICE);
			break;
		case MoreTab:
			view.setText(TAB_TAG_MORE);
			break;
		default:
			break;
		}
		return tabItem;
	}

}
